#!/usr/bin/env node

const csv = require('csv-parser');
const fs = require('fs');
const {promisify} = require('util');
const {argv} = require('yargs');
const {MongoClient, ObjectID} = require('mongodb');
const http = require('https');
const url = require('url');
const shortid = require('shortid');
const path = require('path');
const crypto = require('crypto');

const mkdir = promisify(fs.mkdir);
const writeFile = promisify(fs.writeFile);
const unlink = promisify(fs.unlink);
const exists = promisify(fs.exists);
const sizeOf = promisify(require('image-size'));


function parseValue(s) {
  if (Array.isArray(s)) {
    return s.map(parseValue);
  }

  if (typeof s === 'object') {
    return s;
  }

  if (/^\d+$/.test(s)) {
    return parseInt(s);
  }

  if (s.toLowerCase() === 'true') {
    return true;
  }

  if (s.toLowerCase() === 'false') {
    return false;
  }

  return s;
}

function formatEntity(data) {
  return Object.keys(data).reduce((res, field) => {
    field = field.trim();

    // Ignore
    if (field === '' || field.startsWith('$') || data[field] == null) {
      return res;
    }

    let val = data[field].trim();

    if (val === '') {
      return res;
    }

    // Array
    if (field.endsWith('[]')) {
      field = field.substr(0, field.length - 2);
      val = val.split(',').map(x => x.trim());
    }

    // ObjectID
    if (field.startsWith('@')) {
      try {
        val = Array.isArray(val) ? val.map(x => new ObjectID(x)) : new ObjectID(val);
      } catch (e) {
        console.log(`FORMAT ERROR: Invalid ObjectID: ${val}`);
        throw e;
      }
      field = field.substr(1);

    // Capitalise
    } else if (field.startsWith('!')) {
      val = Array.isArray(val) ? val.map(x => x[0].toUpperCase() + x.substr(1).toLowerCase()) : val[0].toUpperCase() + val.substr(1).toLowerCase();
      field = field.substr(1);
    }

    val = parseValue(val);

    return {
      ...res,
      [field]: val,
    };
  }, {});
}

function delay(ms = 500) {
  return new Promise(resolve => setTimeout(() => resolve(), ms));
}

function download(url, dest) {
  console.log(`downloading ${url}`);
  return new Promise((resolve, reject) => {
    const file = fs.createWriteStream(dest);

    const request = http.get(url, async (response) => {
      if (response.statusCode !== 200) {
        await unlink(dest);
        reject(`Response status was ${response.statusCode}`);
        return;
      }

      response.pipe(file);
    });

    file.on('finish', () => {
      console.log('ok');
      file.close(resolve);
    });

    request.on('error', async (err) => {
      await unlink(dest);
      reject(err);
    });

    file.on('error', async (err) => {
      await unlink(dest);
      reject(err);
    });
  });
}


async function tryDownload(url, dest) {
  for (let _ = 0; _ < 2; _++) {
    try {
      if (!await exists(dest)) {
        await download(url, dest);
        await delay();
      }
    } catch (e) {
      console.log(`DOWNLOAD ERROR: ${e.message}`);
      console.log(e);
      await delay(5000);
    }
  }
}


async function importMaterials(client, options) {
  const materials = [];

  await new Promise((resolve, reject) => {
    fs.createReadStream(options.in, {encoding: 'utf8'})
      .pipe(csv({separator: ','}))
      .on('end', () => resolve())
      .on('data', async (raw) => materials.push(formatEntity(raw)));
  });

  for (const material of materials) {
    const materialURL = material.url || material._url;
    console.log(`Material: ${materialURL}`);

    // extract object id
    const id = crypto.createHash('md5').update(materialURL).digest('hex').substr(0, 24);

    // extract texture file name
    const textureURL = material._texture;
    const textureFilename = 'texture' + path.extname(url.parse(textureURL).pathname);

    // extract images
    const imageURLs = material.images;
    material.images = imageURLs.map(u => {
      const pathname = path.basename(url.parse(u).pathname);
      const newName = crypto.createHash('md5').update(pathname).digest('hex').substr(0, 12);
      return newName + path.extname(pathname);
    });

    // download texture
    await tryDownload(textureURL, `${options.path}/${id}/${textureFilename}`);
    const textureDimension = await sizeOf(`${options.path}/${id}/${textureFilename}`);

    // download images
    for (let i = 0; i < imageURLs.length; i++) {
      await tryDownload(imageURLs[i], `${options.path}/${id}/${material.images[i]}`);
    }

    // create model.json
    if (!await exists(`${options.path}/${id}`)) {
      await mkdir(`${options.path}/${id}`);
    }
    await writeFile(`${options.path}/${id}/model.json`, JSON.stringify({
      texture: {
        filename: textureFilename,
        stretch: false,
        scale: 200,
        size: textureDimension,
        imageSize: textureDimension,
        ...(material.width ? {
          realSize: {width: material.width / 1000}
        } : {}),
      }
    }, null, 2));

    // prepare material object for inserting to database
    Object.keys(material).filter(x => x.startsWith('_')).forEach(x => delete material[x]);

    // create Material record in database
    await client.db(options.db).collection('materials').replaceOne(
      {_id: ObjectID(id)},
      {...material, version: parseInt(options.ver || '1')},
      {upsert: true}
    );

    // create Lot record in database
    if (options.seller) {
      const lotId = crypto.createHash('md5').update(`${options.seller}${materialURL}`).digest('hex').substr(0, 24);
      await client.db(options.db).collection('lots').replaceOne(
          {_id: ObjectID(lotId)},
          {
            url: materialURL,
            sku: material.vendorCode,
            material: ObjectID(id),
            seller: ObjectID(options.seller),
            price: material.price,
          },
          {upsert: true}
      );
    }
  }
}


if (process.env.NODE_ENV !== 'test') {
  (async function () {
    const client = await MongoClient.connect(`mongodb://${argv.host}`);
    let missedOptions;
    switch (argv._[0]) {
      case 'materials':
        missedOptions = ['in', 'path', 'host', 'db', 'seller'].reduce((res, option) => {
          if (argv[option]) {
            return res;
          }
          return [
            ...res,
            option,
          ];
        }, []);

        if (missedOptions.length > 0) {
          console.log(`Missed options: ${missedOptions.map(x => `--${x}`).join(', ')}`);
          return;
        }

        await importMaterials(client, argv);
        break;
      default:
        console.log(`Unknown command: ${argv._[0]}`);
        console.log(`You can use one of the next commands: materials, brands, lots`);
    }
  }()).catch((err) => {
    console.log(err);
  });


}

exports.formatEntity = formatEntity;
exports.parseValue = parseValue;
