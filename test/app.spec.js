const {expect} = require('chai');
const {formatEntity, parseValue} = require('../app');
const {ObjectID} = require('mongodb');

describe('app', () => {
  describe('formatEntity', () => {
    it('should return mongo formatted data', () => {
      const res = formatEntity({
        $url: 'https://odesign.ru/art-4394',
        '!name': 'art Studio 4394',
        '!colors[]': 'Red, blue,  YELLOW',
        '@brand': '5e218981aa1d6588a0020ac8',
      });
      expect(res).eql({
        name: 'Art studio 4394',
        colors: ['Red', 'Blue', 'Yellow'],
        brand: new ObjectID('5e218981aa1d6588a0020ac8'),
      });
    });
  });
});
